package sample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.work.*
import sample.ui.theme.WorkManagerDSLTheme
import sample.work.TestWorker
import workmanager.dsl.*
import java.util.*
import java.util.concurrent.TimeUnit

class SampleActivity : ComponentActivity() {

    private val workManager: WorkManager by lazy { WorkManager.getInstance(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WorkManagerDSLTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    SampleActivityContent(
                        onClickSingleWork = { launchSingleWorker() },
                        onClickPeriodicWork = { launchPeriodicWork() },
                        onClickWorkChain = { launchWorkChain() },
                        onClickComplexWorkChain = { launchWorkChainComplex() },
                        onClickCombinedWorkChain = { launchWorkChainCombined() },
                    )
                }
            }
        }
    }

    private fun launchSingleWorker() {
        workManager.runOnce<TestWorker> {
            // If value set, will run worker as expedited
            expeditedWorkPolicy = OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST

            constraints = TestWorker.CONSTRAINTS
            inputData = TestWorker.getInputData(111)
        }


        // ====================================== Unique Work ======================================

//        workManager.runUniqueOnce<TestWorker>("Unique One Time Name", ExistingWorkPolicy.KEEP) {
//            constraints = TestWorker.CONSTRAINTS
//            inputData = TestWorker.getInputData(222)
//        }
    }

    private fun launchPeriodicWork() {
        workManager.runPeriodically<TestWorker>(10, TimeUnit.MINUTES) {
            constraints = TestWorker.CONSTRAINTS
            inputData = TestWorker.getInputData(222)
            tag = "PeriodicWorker"
        }

        // ====================================== Unique Work ======================================

//        workManager.runUniquePeriodically<TestWorker>("Unique Periodic Name", ExistingPeriodicWorkPolicy.KEEP, 10, TimeUnit.MINUTES) {
//            constraints = TestWorker.CONSTRAINTS
//            inputData = TestWorker.getInputData(222)
//            tag = "PeriodicWorker"
//        }
    }

    private fun launchWorkChain() {
        var workIndex = 1
        var stepTwoId: UUID? = null

        workManager.runChain {
            // Can mark chain as unique
            setUnique("Test Chain", ExistingWorkPolicy.KEEP)

            beginWith<TestWorker> {
                inputData = TestWorker.getInputData(workIndex++)
            }
            stepTwoId = then<TestWorker> {
                inputData = TestWorker.getInputData(workIndex++)
            }
            then<TestWorker> {
                inputData = TestWorker.getInputData(workIndex++)
            }
        }

        workManager.getWorkInfoByIdLiveData(stepTwoId!!).observe(this) {
            // Observe work progress just like you would normally
        }
    }

    private fun launchWorkChainComplex() {
        var workIndex = 1

        workManager.runChain {
            beginWithTogether {
                runOnce<TestWorker> {
                    inputData = TestWorker.getInputData(workIndex++)
                    tag = "First-Worker-1"
                }
                runOnce<TestWorker> {
                    inputData = TestWorker.getInputData(workIndex++)
                    tag = "First-Worker-2"
                }
            }
            then<TestWorker> {
                inputData = TestWorker.getInputData(workIndex++)
                inputMerger = ArrayCreatingInputMerger::class.java
                tags = listOf("A", "B")
                initialDelay(5, TimeUnit.SECONDS)
                backoffCriteria(BackoffPolicy.EXPONENTIAL, 10, TimeUnit.MINUTES)
            }
            thenTogether {
                repeat(3) {
                    runOnce<TestWorker> {
                        inputData = TestWorker.getInputData(workIndex++)
                    }
                }
            }
            then<TestWorker>() // Can still use without configuration lambda
        }
    }

    private fun launchWorkChainCombined() {
        var workIndex = 1

        // You can combine chains together so they run in parallel

        workManager.runChain {
            beginWith<TestWorker> {
                inputData = TestWorker.getInputData(workIndex++)
            }
            then<TestWorker> {
                inputData = TestWorker.getInputData(workIndex++)
            }
            combineWith {
                beginWith<TestWorker> {
                    inputData = TestWorker.getInputData(workIndex++)
                }
                then<TestWorker> {
                    inputData = TestWorker.getInputData(workIndex++)
                }
                combineWith {
                    beginWith<TestWorker> {
                        inputData = TestWorker.getInputData(workIndex++)
                    }
                    then<TestWorker> {
                        inputData = TestWorker.getInputData(workIndex++)
                    }
                }
            }
            then<TestWorker> {
                inputData = TestWorker.getInputData(workIndex++)
            }
        }
    }
}

@Composable
fun SampleActivityContent(onClickSingleWork: () -> Unit,
                          onClickPeriodicWork: () -> Unit,
                          onClickWorkChain: () -> Unit,
                          onClickComplexWorkChain: () -> Unit,
                          onClickCombinedWorkChain: () -> Unit) {

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize(),
    ) {

        Text(
            text = "You should be able to view the work progress with App Inspection & Logcat",
            style = MaterialTheme.typography.subtitle1,
            modifier = Modifier.padding(16.dp),
        )

        Spacer(modifier = Modifier.size(32.dp))

        Box(modifier = Modifier.fillMaxWidth().weight(1f)) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.align(Alignment.Center),
            ) {

                Button(onClick = onClickSingleWork, modifier = Modifier.padding(16.dp)) {
                    Text(
                        text = "Launch Single Work",
                        style = MaterialTheme.typography.button,
                        modifier = Modifier.padding(4.dp)
                    )
                }

                Button(onClick = onClickPeriodicWork, modifier = Modifier.padding(16.dp)) {
                    Text(
                        text = "Launch Periodic Work",
                        style = MaterialTheme.typography.button,
                        modifier = Modifier.padding(4.dp)
                    )
                }

                Button(onClick = onClickWorkChain, modifier = Modifier.padding(16.dp)) {
                    Text(
                        text = "Launch Work Chain",
                        style = MaterialTheme.typography.button,
                        modifier = Modifier.padding(4.dp)
                    )
                }

                Button(onClick = onClickComplexWorkChain, modifier = Modifier.padding(16.dp)) {
                    Text(
                        text = "Launch Complex Work Chain",
                        style = MaterialTheme.typography.button,
                        modifier = Modifier.padding(4.dp)
                    )
                }

                Button(onClick = onClickCombinedWorkChain, modifier = Modifier.padding(16.dp)) {
                    Text(
                        text = "Launch Combined Work Chain",
                        style = MaterialTheme.typography.button,
                        modifier = Modifier.padding(4.dp)
                    )
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun SampleActivityPreview() {
    WorkManagerDSLTheme {
        SampleActivityContent(
            onClickSingleWork = {},
            onClickPeriodicWork = {},
            onClickWorkChain = {},
            onClickComplexWorkChain = {},
            onClickCombinedWorkChain = {},
        )
    }
}