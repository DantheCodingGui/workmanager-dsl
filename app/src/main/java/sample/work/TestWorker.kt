package sample.work

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.work.*
import kotlinx.coroutines.delay
import uk.co.danielbaxter.workmanagerdsl.R

class TestWorker(private val context: Context, params: WorkerParameters)
    : CoroutineWorker(context, params) {

    private val id: Int by lazy {
        inputData.getInt(DATA_WORKER_ID, -1)
    }

    private val previousIds: IntArray by lazy {
        inputData.getIntArray(DATA_PREVIOUS_IDS) ?: intArrayOf()
    }

    override suspend fun doWork(): Result {
        delay(1000L)
        Log.i("TestWorker", "Doing Work${if (id != -1) " with ID $id" else ""}!!")
        if (previousIds.isNotEmpty())
            Log.i("TestWorker", "Previous workers were ${previousIds.joinToString()}")
        delay(1000L)

        return Result.success(
            workDataOf(DATA_PREVIOUS_IDS to id)
        )
    }

    override suspend fun getForegroundInfo(): ForegroundInfo {
        val notificationId = 12345
        val channelId = "1234567890"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                "1234567890",
                "Test Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            ).apply { this.description = description }

            val notificationManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

        val notification = NotificationCompat.Builder(applicationContext, channelId)
            .setContentTitle("Test Expedite/Foreground Work")
            .setContentText("Doing Work...")
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setOngoing(true)
            .build()

        return ForegroundInfo(notificationId, notification)
    }

    companion object {
        private const val DATA_WORKER_ID = "dataWorkerId"
        private const val DATA_PREVIOUS_IDS = "dataPreviousIds"

        val CONSTRAINTS = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        fun getInputData(testValueOne: Int) = workDataOf(
            DATA_WORKER_ID to testValueOne,
        )
    }
}