[![](https://jitpack.io/v/uk.co.danielbaxter/WorkManager-DSL.svg?style=flat-square)](https://jitpack.io/#uk.co.danielbaxter/WorkManager-DSL)

# WorkManager DSL

A Kotlin Domain-Specific Language to make working with Android's WorkManager, especially when using complex chains, much easier.

Setup
-----  

Add the JitPack repository to your project level `build.gradle`:

```groovy  
	allprojects {  
		repositories {  
			... 
			maven { url "https://jitpack.io" }  
		}
	}  
```  

OR, if you are using Android Gradle Plugin 7+, add it to your settings.gradle:

```groovy
    dependencyResolutionManagement {
        ...
        repositories {
            ...
            maven { url "https://jitpack.io" }
        }
    }
```

and the library to your app/module-level `build.gradle`:

```groovy  
	dependencies {  
		implementation 'uk.co.danielbaxter:workmanager-dsl:1.0.0-alpha3'
	} 
```  

Finally, complete whatever normal setup you'd do when using WorkManager (initialisation, configuration provider etc.)

Usage
-----

#### Single Workers

The library provides extension methods on WorkManager to simplify scheduling workers.

```kotlin  
	// No Configuration  
	workManagerInstance.runOnce<ExampleWorker>()  
  
	workManagerInstance.runOnce<ExampleWorker> {  
		// Configuration Methods 
		constraints = Constraints.Builder()
						.setRequiredNetworkType(NetworkType.CONNECTED)
						.build() 
		inputData = workDataOf("Test Value" to 5) 
		...
	 }  
  
	workManagerInstance.runPeriodically<ExampleWorker>(10, TimeUnit.MINUTES)  
  
	workManagerInstance.runPeriodically<ExampleWorker>(10, TimeUnit.MINUTES) {  
		tag = "PeriodicWorker"
	}  
```  

#### Chains

All of the current features of WorkManager chains are supported here.

**NOTE**: Periodic work requests are not supported in chains, so they can't be used here either

```kotlin  
	workManagerInstance.runChain {  
		// Can optionally mark chain as unique 
		setUnique("Test Chain Name", ExistingWorkPolicy.KEEP)
		  
		beginWithTogether { 
			runOnce<ExampleWorker>() 
			runOnce<ExampleWorker>() 
		} 
		// These methods all return the UUID of the work request, allowing them to be observed
		stepTwoId = then<ExampleWorker>() 
		thenTogether { 
			runOnce<ExampleWorker>() 
			runOnce<ExampleWorker>() 
		} 
		then<ExampleWorker>()
	}  
```  

Chains can also be combined together:

```kotlin  
	workManagerInstance.runChain {  
		beginWith<ExampleWorker> { 
			tag = "A" 
		}  
		then<ExampleWorker> {  
			tag = "B" 
		} 
		combineWith { 
			beginWith<ExampleWorker> { 
				tag = "C" 
			}  
			then<ExampleWorker> {  
				tag = "D" 
			} 
			combineWith { 
				beginWith<ExampleWorker> { 
					tag = "E" 
				}  
				then<ExampleWorker> {  
					tag = "F" 
				} 
			} 
		}  
		then<ExampleWorker> {  
			tag = "G" 
		}
	}  
```  

The visual representation of that chain is as follows:

    A    C    E
    |    |    |
    B    D    F
     \   |   /
      \  |  /
         G

#### Properties

Under the hood, the same `OneTimeWorkRequest.Builder()` and `PeriodicWorkRequest.Builder()` are still used.  
But to provide a cleaner and more readable API, all the configuration values currently supported in   
WorkManager have had extension properties added (e.g. `tag = x` instead of `setTag(x)`).

Samples
-----

The library contains sample usage of the features listed above.

License
-------

    Copyright 2021 Daniel Baxter

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.