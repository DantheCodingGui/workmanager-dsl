package workmanager.dsl

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.work.*
import java.time.Duration
import java.util.concurrent.TimeUnit

var WorkRequest.Builder<*, *>.constraints: Constraints
    get() = throw UnsupportedOperationException("Can't get work request constraints")
    set(value) {
        setConstraints(value)
    }

var WorkRequest.Builder<*, *>.inputData: Data
    get() = throw UnsupportedOperationException("Can't get work request input data")
    set(value) {
        setInputData(value)
    }

var WorkRequest.Builder<*, *>.tag: String
    get() = throw UnsupportedOperationException("Can't get work request tags")
    set(value) {
        tags = listOf(value)
    }

var WorkRequest.Builder<*, *>.tags: List<String>
    get() = throw UnsupportedOperationException("Can't get work request tags")
    set(value) {
        value.forEach { tag -> addTag(tag) }
    }

var WorkRequest.Builder<*, *>.initialDelay: Duration
    get() = throw UnsupportedOperationException("Can't get work request initial delay")
    @RequiresApi(Build.VERSION_CODES.O)
    set(value) {
        setInitialDelay(value)
    }

var WorkRequest.Builder<*, *>.expeditedWorkPolicy: OutOfQuotaPolicy
    get() = throw UnsupportedOperationException("Can't get work request out-of-quota policy")
    set(value) {
        setExpedited(value)
    }

inline fun WorkRequest.Builder<*, *>.initialDelay(duration: Long, timeUnit: TimeUnit) {
    setInitialDelay(duration, timeUnit)
}

inline fun WorkRequest.Builder<*, *>.backoffCriteria(backoffPolicy: BackoffPolicy, backoffDelay: Long, timeUnit: TimeUnit) {
    setBackoffCriteria(backoffPolicy, backoffDelay, timeUnit)
}

@RequiresApi(Build.VERSION_CODES.O)
inline fun WorkRequest.Builder<*, *>.backoffCriteria(backoffPolicy: BackoffPolicy, duration: Duration) {
    setBackoffCriteria(backoffPolicy, duration)
}
