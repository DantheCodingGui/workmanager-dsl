package workmanager.dsl

import androidx.work.ListenableWorker
import androidx.work.OneTimeWorkRequest

internal inline fun buildRequest(workerClass: Class<out ListenableWorker?>, builder: (OneTimeWorkRequest.Builder).() -> Unit) =
    OneTimeWorkRequest.Builder(workerClass).apply(builder).build()