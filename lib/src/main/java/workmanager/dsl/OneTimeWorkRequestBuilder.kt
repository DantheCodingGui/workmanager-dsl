package workmanager.dsl

import androidx.work.InputMerger
import androidx.work.OneTimeWorkRequest

var OneTimeWorkRequest.Builder.inputMerger: Class<out InputMerger>
    get() = throw UnsupportedOperationException("Can't get work request input merger")
    set(value) {
        setInputMerger(value)
    }