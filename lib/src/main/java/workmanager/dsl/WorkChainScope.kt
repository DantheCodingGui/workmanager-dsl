package workmanager.dsl

import androidx.work.ListenableWorker
import androidx.work.OneTimeWorkRequest
import workmanager.dsl.scope.WorkChainScope
import workmanager.dsl.scope.WorkScope

inline fun <reified W: ListenableWorker> WorkChainScope.beginWith(noinline builder: (@WorkScope OneTimeWorkRequest.Builder).() -> Unit = {}) =
    beginWith(W::class.java, builder)

inline fun <reified W: ListenableWorker> WorkChainScope.then(noinline builder: (@WorkScope OneTimeWorkRequest.Builder).() -> Unit = {}) =
    then(W::class.java, builder)