package workmanager.dsl.scope

import androidx.work.ListenableWorker
import androidx.work.OneTimeWorkRequest
import workmanager.dsl.buildRequest
import java.util.*

@WorkScope
interface ConcurrentWorkStepsScope {
    fun runOnce(workerClass: Class<out ListenableWorker?>,
                builder: (@WorkScope OneTimeWorkRequest.Builder).() -> Unit): UUID
}

internal class ConcurrentWorkStepsScopeImpl : ConcurrentWorkStepsScope {
    val steps = mutableListOf<OneTimeWorkRequest>()

    override fun runOnce(workerClass: Class<out ListenableWorker?>,
                         builder: (OneTimeWorkRequest.Builder).() -> Unit): UUID {

        val request = buildRequest(workerClass, builder)

        steps.add(request)
        return request.id
    }
}