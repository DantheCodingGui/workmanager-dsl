package workmanager.dsl.scope

import android.annotation.SuppressLint
import androidx.work.*
import workmanager.dsl.buildRequest
import java.util.*

@WorkScope
interface WorkChainScope {
    fun setUnique(name: String, existingWorkPolicy: ExistingWorkPolicy)

    fun beginWith(workerClass: Class<out ListenableWorker?>, builder: (@WorkScope OneTimeWorkRequest.Builder).() -> Unit): UUID
    fun beginWithTogether(stepsBuilder: (@WorkScope ConcurrentWorkStepsScope).() -> Unit)
    fun then(workerClass: Class<out ListenableWorker?>, builder: (@WorkScope OneTimeWorkRequest.Builder).() -> Unit): UUID
    fun thenTogether(stepsBuilder: (@WorkScope ConcurrentWorkStepsScope).() -> Unit)

    fun combineWith(builder: (@WorkScope WorkChainScope).() -> Unit)
}

@SuppressLint("EnqueueWork")
internal class WorkChainScopeImpl(private val workManager: WorkManager): WorkChainScope {

    var workContinuation: WorkContinuation? = null
    var startedChain = false

    private var isUnique = false
    private var uniqueWorkChainName: String? = null
    private var uniqueExistingWorkPolicy: ExistingWorkPolicy? = null

    override fun setUnique(name: String, existingWorkPolicy: ExistingWorkPolicy) {
        require(workContinuation == null) { "You must call setUnique before any other chain configuration" }

        isUnique = true

        uniqueWorkChainName = name
        uniqueExistingWorkPolicy = existingWorkPolicy
    }

    override fun beginWith(workerClass: Class<out ListenableWorker?>,
                           builder: (OneTimeWorkRequest.Builder).() -> Unit): UUID {

        require(!startedChain) { "You can only call beginWith once" }
        require(workContinuation == null) { "beginWith must be the first work call in the chain" }
        startedChain = true

        val request = buildRequest(workerClass, builder)
        workContinuation = if (isUnique)
            workManager.beginUniqueWork(uniqueWorkChainName!!, uniqueExistingWorkPolicy!!, request)
        else
            workManager.beginWith(request)

        return request.id
    }

    override fun beginWithTogether(stepsBuilder: (ConcurrentWorkStepsScope).() -> Unit) {
        require(!startedChain) { "You can only call beginWith once" }
        require(workContinuation == null) { "beginWith must be the first work call in the chain" }
        startedChain = true

        val requests = ConcurrentWorkStepsScopeImpl().apply(stepsBuilder).steps
        workContinuation = if (isUnique)
            workManager.beginUniqueWork(uniqueWorkChainName!!, uniqueExistingWorkPolicy!!, requests)
        else
            workManager.beginWith(requests)
    }

    override fun then(workerClass: Class<out ListenableWorker?>,
                      builder: (OneTimeWorkRequest.Builder).() -> Unit): UUID {
        require(workContinuation != null) { "beginWith hasn't been called" }

        val request = buildRequest(workerClass, builder)
        workContinuation = workContinuation!!.then(request)

        return request.id
    }

    override fun thenTogether(stepsBuilder: (ConcurrentWorkStepsScope).() -> Unit) {
        require(workContinuation != null) { "beginWith hasn't been called" }

        workContinuation = workContinuation!!.then(ConcurrentWorkStepsScopeImpl().apply(stepsBuilder).steps)
    }

    override fun combineWith(builder: (WorkChainScope).() -> Unit) {
        val otherChain = WorkChainScopeImpl(workManager).apply(builder).workContinuation
        require(workContinuation != null && otherChain != null) { "Cannot combine an empty chain" }

        workContinuation = WorkContinuation.combine(listOf(
            workContinuation!!,
            otherChain
        ))
    }

    fun enqueue() {
        require(workContinuation != null) { "Chain request with no steps??" }

        workContinuation!!.enqueue()
    }
}