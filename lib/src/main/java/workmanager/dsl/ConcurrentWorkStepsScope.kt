package workmanager.dsl

import androidx.work.ListenableWorker
import androidx.work.OneTimeWorkRequest
import workmanager.dsl.scope.ConcurrentWorkStepsScope
import workmanager.dsl.scope.WorkScope

inline fun <reified W: ListenableWorker> ConcurrentWorkStepsScope.runOnce(noinline builder: (@WorkScope OneTimeWorkRequest.Builder).() -> Unit = {}) =
    runOnce(W::class.java, builder)