package workmanager.dsl

import androidx.work.*
import workmanager.dsl.scope.WorkChainScope
import workmanager.dsl.scope.WorkChainScopeImpl
import workmanager.dsl.scope.WorkScope
import java.time.Duration
import java.util.*
import java.util.concurrent.TimeUnit

inline fun <reified W: ListenableWorker> WorkManager.runOnce(builder: OneTimeWorkRequest.Builder.() -> Unit = {}): UUID =
    OneTimeWorkRequestBuilder<W>().apply(builder).build().let { request ->
        enqueue(request)
        request.id
    }

inline fun <reified W: ListenableWorker> WorkManager.runUniqueOnce(name: String,
                                                                   existingWorkPolicy: ExistingWorkPolicy,
                                                                   builder: OneTimeWorkRequest.Builder.() -> Unit = {}): UUID =
    OneTimeWorkRequestBuilder<W>().apply(builder).build().let { request ->
        enqueueUniqueWork(name, existingWorkPolicy, request)
        request.id
    }


inline fun <reified W: ListenableWorker> WorkManager.runPeriodically(repeatInterval: Duration,
                                                                     builder: PeriodicWorkRequest.Builder.() -> Unit = {}): UUID =
    PeriodicWorkRequestBuilder<W>(repeatInterval).apply(builder).build().let { request ->
        enqueue(request)
        request.id
    }

inline fun <reified W: ListenableWorker> WorkManager.runPeriodically(repeatInterval: Long,
                                                                     repeatIntervalTimeUnit: TimeUnit,
                                                                     builder: PeriodicWorkRequest.Builder.() -> Unit = {}): UUID =
    PeriodicWorkRequestBuilder<W>(repeatInterval, repeatIntervalTimeUnit).apply(builder).build().let { request ->
        enqueue(request)
        request.id
    }

inline fun <reified W: ListenableWorker> WorkManager.runPeriodically(repeatInterval: Duration,
                                                                     flexTimeInterval: Duration,
                                                                     builder: PeriodicWorkRequest.Builder.() -> Unit = {}): UUID =
    PeriodicWorkRequestBuilder<W>(repeatInterval, flexTimeInterval).apply(builder).build().let { request ->
        enqueue(request)
        request.id
    }

inline fun <reified W: ListenableWorker> WorkManager.runPeriodically(repeatInterval: Long,
                                                                     repeatIntervalTimeUnit: TimeUnit,
                                                                     flexTimeInterval: Long,
                                                                     flexTimeIntervalUnit: TimeUnit,
                                                                     builder: PeriodicWorkRequest.Builder.() -> Unit = {}): UUID =
    PeriodicWorkRequestBuilder<W>(repeatInterval, repeatIntervalTimeUnit,
        flexTimeInterval, flexTimeIntervalUnit).apply(builder).build().let { request ->
        enqueue(request)
        request.id
    }



inline fun <reified W: ListenableWorker> WorkManager.runUniquePeriodically(name: String,
                                                                           existingWorkPolicy: ExistingPeriodicWorkPolicy,
                                                                           repeatInterval: Duration,
                                                                           builder: PeriodicWorkRequest.Builder.() -> Unit = {}): UUID =
    PeriodicWorkRequestBuilder<W>(repeatInterval).apply(builder).build().let { request ->
        enqueueUniquePeriodicWork(name, existingWorkPolicy, request)
        request.id
    }

inline fun <reified W: ListenableWorker> WorkManager.runUniquePeriodically(name: String,
                                                                           existingWorkPolicy: ExistingPeriodicWorkPolicy,
                                                                           repeatInterval: Long,
                                                                           repeatIntervalTimeUnit: TimeUnit,
                                                                           builder: PeriodicWorkRequest.Builder.() -> Unit = {}): UUID =
    PeriodicWorkRequestBuilder<W>(repeatInterval, repeatIntervalTimeUnit).apply(builder).build().let { request ->
        enqueueUniquePeriodicWork(name, existingWorkPolicy, request)
        request.id
    }


inline fun <reified W: ListenableWorker> WorkManager.runUniquePeriodically(name: String,
                                                                           existingWorkPolicy: ExistingPeriodicWorkPolicy,
                                                                           repeatInterval: Duration,
                                                                           flexTimeInterval: Duration,
                                                                           builder: PeriodicWorkRequest.Builder.() -> Unit = {}): UUID =
    PeriodicWorkRequestBuilder<W>(repeatInterval, flexTimeInterval).apply(builder).build().let { request ->
        enqueueUniquePeriodicWork(name, existingWorkPolicy, request)
        request.id
    }

inline fun <reified W: ListenableWorker> WorkManager.runUniquePeriodically(name: String,
                                                                           existingWorkPolicy: ExistingPeriodicWorkPolicy,
                                                                           repeatInterval: Long,
                                                                           repeatIntervalTimeUnit: TimeUnit,
                                                                           flexTimeInterval: Long,
                                                                     flexTimeIntervalUnit: TimeUnit,
                                                                     builder: PeriodicWorkRequest.Builder.() -> Unit = {}): UUID =
    PeriodicWorkRequestBuilder<W>(repeatInterval, repeatIntervalTimeUnit,
        flexTimeInterval, flexTimeIntervalUnit).apply(builder).build().let { request ->
        enqueueUniquePeriodicWork(name, existingWorkPolicy, request)
        request.id
    }



fun WorkManager.runChain(builder: (@WorkScope WorkChainScope).() -> Unit) =
    WorkChainScopeImpl(this).apply(builder).enqueue()